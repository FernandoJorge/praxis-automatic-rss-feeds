package sendrssfeeds;

import Dal.*;
import Utils.*;
import java.io.File;
import java.io.FileWriter;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

/**
 *
 * @author Janus
 */
public class SendRSSFeeds {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        // GETS MYSQL DATABASE CONNECTION OBJECT
        DataBaseAccess dbConnection = FactoryDataBaseAccess.getDataBaseAccessObject("MYSQL");

        if (dbConnection == null) {

            return;
        }

        // OPENS DATABASE'S CONNECTION
        if (dbConnection.openConnection("localhost:3306", "praxis", "root", "")) {

            String SQL = "SELECT * FROM feed_information";

            ResultSet rs = dbConnection.select(SQL);

            // LAST FEED UPDATE
            int lastFeedID = -1;
            String xmlPath = "";
            int maxDescription = -1;
            float porcMinDescription = -1;
            int validationDate = -1;

            try {

                while (rs.next()) {

                    lastFeedID = rs.getInt("last_feed_display");
                    xmlPath = rs.getString("feed_xml_path").trim();
                    maxDescription = rs.getInt("max_description");
                    porcMinDescription = rs.getFloat("min_perc_description");
                    validationDate = rs.getInt("validation_date");
                }

            } catch (Exception ex) {

                Logs.writeLog("(AUTOMATIC FEEDS UPDATE) UNABLE TO READ FEED INFORMATION", true);
                dbConnection.closeConnection();
                return;
            }

            // TESTS IF THERE ARE CORRECT INFORMATION
            if (lastFeedID == -1) {

                Logs.writeLog("(AUTOMATIC FEEDS UPDATE) THERE ARE NO PROPOSALS TO SEND RSS FEED UPDATE", true);
                dbConnection.closeConnection();
                return;
            }

            if (xmlPath.equals("")) {

                Logs.writeLog("(AUTOMATIC FEEDS UPDATE) THERE IS NO XML RSS FEED FILE PATH INFORMATION", true);
                dbConnection.closeConnection();
                return;
            }

            if (maxDescription == -1 || porcMinDescription == -1) {

                Logs.writeLog("(AUTOMATIC FEEDS UPDATE) THERE IS NO INFORMATION FOR PORPOSAL DESCRIPTION SIZE", true);
                dbConnection.closeConnection();
                return;
            }

            // GETS ALL THE PROPOSALS GREATER THAN THE LAST PROPOSAL ID RSS FEEDED
            if (validationDate == 0) {
                SQL = "SELECT id , title , description FROM proposal WHERE id > " + lastFeedID + " ORDER BY id ASC";
            } else {
                if (validationDate == 1) {

                    SQL = "SELECT id , title , description FROM proposal WHERE id > " + lastFeedID + " AND ( "
                            + "( validTo > startDate AND startDate > CURDATE() ) "
                            + "OR ( startDate > validTo AND validTo > CURDATE() ) "
                            + ") "
                            + "ORDER BY id ASC";
                }
            }

            rs = dbConnection.select(SQL);

            int size = 0;

            try {
                rs.last();
                size = rs.getRow();
                rs.beforeFirst();
            } catch (Exception ex) {

                dbConnection.closeConnection();
                return;
            }

            if (size == 0) {

                dbConnection.closeConnection();
                return;
            }

            // CREATES HTML EMAIL UPDATE CONTENT
            String htmlMailContent = createHtmlUpdateContent(rs, maxDescription, porcMinDescription);

            if (htmlMailContent.equals("error")) {

                dbConnection.closeConnection();
                return;
            }

            try {
                rs.beforeFirst();
            } catch (Exception ex) {

                dbConnection.closeConnection();
                return;
            }

            // CREATES XML RSS FEED FILE CONTENT
            String xmlRssFeedContent = createXmlUpdateContent(rs, maxDescription, porcMinDescription);

            if (xmlRssFeedContent.equals("error")) {

                dbConnection.closeConnection();
                return;
            }

            // LAST PROPOSAL ID FEEDED
            int lastID;

            try {
                rs.last();
                lastID = rs.getInt("id");
            } catch (Exception ex) {

                Logs.writeLog("(AUTOMATIC FEEDS UPDATE) IT WAS NOT POSSIBLE TO GET THE LAST PROPOSAL ID FEEDED", true);
                dbConnection.closeConnection();
                return;
            }

            // GETS ALL THE EMAIL ADDRESSES OF COMMUNITY
            SQL = "SELECT email FROM feed_community ORDER BY email ASC";

            ArrayList<String> emailList = new ArrayList<String>();

            rs = dbConnection.select(SQL);

            try {
                while (rs.next()) {

                    emailList.add(rs.getString("email").trim());
                }
            } catch (Exception ex) {

                Logs.writeLog("(AUTOMATIC FEEDS UPDATE) IT WAS NOT POSSIBLE TO READ THE COMMUNTY'S EMAILS : " + ex.getCause(), true);
                dbConnection.closeConnection();
                return;
            }

            if (emailList.size() == 0) {

                dbConnection.closeConnection();
                return;
            }

            // GETS EMAIL SETTINGS
            int port = -1;
            String fromAddress = "";
            String smtpServer = "";
            boolean authentication = false;
            String SSLAuthentication = "";
            String username = "";
            String password = "";

            SQL = "SELECT * FROM email_settings";

            rs = dbConnection.select(SQL);

            try {
                while (rs.next()) {

                    port = rs.getInt("smtp_port_number");
                    fromAddress = rs.getString("from_address").trim();
                    smtpServer = rs.getString("smtp_server").trim();
                    SSLAuthentication = rs.getString("ssl_authentication").trim();
                    username = rs.getString("username").trim();
                    password = rs.getString("password").trim();
                }
            } catch (Exception ex) {

                Logs.writeLog("(AUTOMATIC FEEDS UPDATE) IT WAS NOT POSSIBLE TO READ THE EMAIL SETTINGS : " + ex.getCause(), true);
                dbConnection.closeConnection();
                return;
            }

            if (fromAddress.equals("") || smtpServer.equals("") || port == -1 || SSLAuthentication.equals("")) {

                Logs.writeLog("(AUTOMATIC FEEDS UPDATE) INSUFICIENT EMAIL SETTINGS", true);
                dbConnection.closeConnection();
                return;
            }

            // SEND EMAIL MESSAGE
            Email email = new Email(fromAddress, smtpServer, port, SSLAuthentication, username, password);

            if (!email.sendEmail(emailList, "PRAXIS Projects/Internships Update", htmlMailContent)) {

                dbConnection.closeConnection();
                return;
            }

            // CREATE XML RSS FEED UPDATE FILE
            try {

                File file = new File(xmlPath);

                FileWriter writer = new FileWriter(file);
                writer.write(xmlRssFeedContent);
                writer.close();

            } catch (Exception ex) {

                Logs.writeLog("(AUTOMATIC FEEDS UPDATE) IT WAS NOT POSSIBLE TO CREATE XML RSS FEED UPDATE FILE : " + ex.getCause(), true);
                dbConnection.closeConnection();
                return;
            }

            // UPDATES THE ID OF THE LAST PROPOSAL FEEDED
            SQL = "UPDATE feed_information SET last_feed_display = " + lastID + " WHERE last_feed_display = " + lastFeedID;

            if (!dbConnection.update(SQL)) {

                Logs.writeLog("(AUTOMATIC FEEDS UPDATE) EMAIL MESSAGE WITH LATEST PROJECTS/INTERNSHIPS UPDATE WAS SENT BUT IT WAS NOT POSSIBLE TO UPDATE LAST PROPOSAL ID FEED IN THE DATABASE", true);
                dbConnection.closeConnection();
                return;
            }

            // REPORT SUCCESS
            Logs.writeLog("(AUTOMATIC FEEDS UPDATE) EMAIL MESSAGE WITH LATEST PROJECTS/INTERNSHIPS UPDATE WAS SENT", false);

            // CLOSE DATABASE'S CONNECTION
            dbConnection.closeConnection();

        }

    }

    /**
     *
     * GENERATES HTML CONTENT TO SEND BY EMAIL
     *
     * @param rs - Records of all proposals to send as feed update
     * @return - Html content if success | 'error' message if it fails
     */
    private static String createHtmlUpdateContent(ResultSet rs, int maxDescription, float porcMinDescription) {

        String mailContent = "<!DOCTYPE html>"
                + "<html>"
                + "<head>"
                + "<meta charset='UTF-8'>"
                + "<meta name='viewport' content='width=device-width, initial-scale=1.0'>"
                + "<title>PRAXIS PROJECT/INTERSHIPS UPDATE</title>"
                + "</head>"
                + "<body>"
                + "<h1>PRAXIS PROJECTS/INTERNSHIPS UPDATE</h1>"
                + "<br><br><br>";

        try {
            while (rs.next()) {

                String title = rs.getString("title");

                if (title == null) {
                    title = "THERE'S NO TITLE AVAILABLE";
                }

                String link = "http://www.praxisnetwork.eu/proposal/id/" + rs.getInt("id");
                String description = rs.getString("description");

                if (description != null) {
                    description = description.replaceAll("\\<.*?\\>", "");
                    int strSize = description.length();

                    if (strSize >= maxDescription) {

                        description = description.substring(0, maxDescription - 1);
                    } else {

                        int endIndex = (int) (porcMinDescription * strSize);
                        description = description.substring(0, endIndex);
                    }
                } else {

                    description = "THERE'S NO DESCRIPTION AVAILABLE";
                }

                mailContent += "<h2><u>" + title + "</u></h2>"
                        + "<p><b>" + description + "...(click the link below)</b></p>"
                        + "<br><br>"
                        + "<a href='" + link + "' target='_blank'>See full proposal</a>"
                        + "<br><br>"
                        + "<hr>";
            }
        } catch (Exception ex) {

            Logs.writeLog("(AUTOMATIC FEEDS UPDATE) IT WAS NOT POSSIBLE TO CREATE HTML MAIL UPDATE CONTENT : " + ex.getCause(), true);
            return "error";
        }

        mailContent += "</body>"
                + "</html>";

        return mailContent;
    }

    /**
     *
     * GENERATES XML RSS FEED UPDATE FILE CONTENT
     *
     * @param rs - REcords of all proposals to sena as feed update
     * @return - Xml content if success | 'error' message if it fails
     */
    private static String createXmlUpdateContent(ResultSet rs, int maxDescription, float porcMinDescription) {

        ArrayList<String> items = new ArrayList<String>();

        SimpleDateFormat localDf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Calendar cal = Calendar.getInstance();
        String date = localDf.format(cal.getTime()).toString();

        try {

            while (rs.next()) {

                String title = rs.getString("title");

                if (title == null) {
                    title = "THERE'S NO TITLE AVAILABLE";
                }

                String link = "http://www.praxisnetwork.eu/proposal/id/" + rs.getInt("id");
                String description = rs.getString("description");

                if (description != null) {

                    //description = description.replaceAll("\\<.*?\\>", "");
                    int strSize = description.length();

                    if (strSize >= maxDescription) {

                        description = description.substring(0, maxDescription - 1);
                    } else {

                        int endIndex = (int) (porcMinDescription * strSize);
                        description = description.substring(0, endIndex);
                    }

                } else {

                    description = "THERE'S NO DESCRIPTION AVAILABLE";
                }

                String item = "<item>"
                        + "<title>" + title + "</title>"
                        + "<link>" + link + "</link>"
                        + "<description>" + description + "</description>"
                        + "</item>";

                items.add(item);
            }

        } catch (Exception ex) {

            Logs.writeLog("(AUTOMATIC FEEDS UPDATE) IT WAS NOT POSSIBLE TO CREATE XML RSS FEED UPDATE CONTENT : " + ex.getCause(), true);
            return "error";
        }

        String xmlContent = "<?xml version='1.0' encoding='utf-8'?>"
                + "<rss version='2.0'>"
                + "<channel>"
                + "<title>PRAXIS</title>"
                + "<link>http://www.praxisnetwork.eu/</link>"
                + "<description>Praxis is the European Centre for Project/Internship Excellence.</description>"
                + "<pubDate>" + date + "</pubDate>";

        for (int i = 0; i < items.size(); i++) {

            xmlContent += items.get(i);
        }

        xmlContent += "</channel>"
                + "</rss>";

        return xmlContent;
    }

}
