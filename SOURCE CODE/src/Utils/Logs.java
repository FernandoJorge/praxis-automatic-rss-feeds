package Utils;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * CLASS TO REPORT EVENTS/ERRORS
 */
public class Logs {

    /**
     *
     * SAVE EVENT/ERROR LOG
     *
     * @param log - Log description
     * @param error - true if is an error log | false if is an event log
     * @return true if success | false if it fails
     */
    public static boolean writeLog(String log, boolean error) {

        File file = null;

        // OPENS FILE TO WRITE LOG
        try {

            if (!error) {

                file = new File("C:/xampp/htdocs/PRAXIS/FILES/LOGS/logs_events_praxis.txt");
            } else {

                file = new File("C:/xampp/htdocs/PRAXIS/FILES/LOGS/logs_errors_praxis.txt");
            }
        } catch (Exception ex) {

            return false;
        }

        String eventLog = "";
        SimpleDateFormat localDf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Calendar cal = Calendar.getInstance();
        String date = localDf.format(cal.getTime()).toString();

        // SETS LOG MESSAGE
        if (!error) {

            eventLog = "[ " + date + " ] [ MESSAGE EVENT: " + log + " ];\n";
        } else {

            eventLog = "[ " + date + " ] [ MESSAGE ERROR: " + log + " ];\n";
        }

        // WRITES LOG MESSAGE
        try {

            FileWriter fw = new FileWriter(file, true);
            fw.write(eventLog);
            fw.close();
        } catch (Exception ex) {

            return false;
        }

        return true;

    }

}
