/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Utils;

import java.util.*;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

/**
 *
 * CLASS FOR SENDING EMAIL FEED UPDATE MESSAGES
 */
public class Email {

    private int port;
    private String fromAddress;
    private String smtpServer;
    private String SSLAuthentication;
    private String username;
    private String password;

    /**
     * 
     * CONSTRUCTOR
     * 
     * @param fromAddress  - Email address of the sender
     * @param smtpServer - Simple Mail Transfer Protocol Server address
     * @param port - SMTP port number
     * @param SSLAuthentication - SSL authentication type
     * @param username - Authentication´s username
     * @param password - Authentication´s password
     */
    public Email(String fromAddress, String smtpServer, int port, String SSLAuthentication, String username, String password) {

        this.fromAddress = fromAddress;
        this.smtpServer = smtpServer;
        this.port = port;
        this.SSLAuthentication = SSLAuthentication;
        this.username = username;
        this.password = password;
    }

    /**
     * 
     * SEND EMAIL FEED UPDATE MESSAGE
     * 
     * @param toAddress - List of recipientes to send email message
     * @param subject - Email header´s subject
     * @param body - Email content message(only prepared for Html content)
     * @return - true if success | false if it fails
     */
    public boolean sendEmail(ArrayList<String> toAddress, String subject, String body) {

        // GETS PROPERTIES OBJECT
        Properties properties = new Properties();

        // SETS EMAIL SERVER
        properties.put("mail.smtp.host", this.smtpServer);
        properties.put("mail.smtp.port", this.port);

        // SETS AUTHENTICATION IF NEEDED
        Authentication authenticator = null;

        if (this.SSLAuthentication.equals("SSL/TLS")) {

            properties.put("mail.smtp.ssl.enable", true);
            properties.put("mail.smtp.auth", true);

            authenticator = new Authentication(this.username, this.password);
        }

        if (this.SSLAuthentication.equals("STARTTLS")) {

            properties.put("mail.smtp.starttls.enable", "true");
            properties.put("mail.smtp.auth", true);

            authenticator = new Authentication(this.username, this.password);
        }

        // GETS DEFAULT SESSION OBJECT
        Session session = Session.getInstance(properties, authenticator);

        // CREATES A DEFAULT MIME MESSAGE OBJECT
        MimeMessage message = new MimeMessage(session);

        // CREATES A MULTIPART OBJECT FOR TEXT & HTML CONTENT
        Multipart multiPart = new MimeMultipart("MultiPart");

        try {

            // SETS SENDER´S ADDRESS
            message.setFrom(new InternetAddress(this.fromAddress));

            // ADD ALL THE RECIPIENTS ADDRESSES
            for (int i = 0; i < toAddress.size(); i++) {

                message.addRecipient(Message.RecipientType.BCC, new InternetAddress(toAddress.get(i)));
            }

            // SETS THE SUBJECT OF THE EMAIL
            message.setSubject(subject);

            // SET SEND DATE MESSAGE
            message.setSentDate(new Date());

            // HTML PART
            MimeBodyPart HTML = new MimeBodyPart();
            HTML.setContent(body, "text/html");

            // SETS THE BODY OF THE EMAIL
            multiPart.addBodyPart(HTML);
            message.setContent(multiPart);

            // SEND EMAIL MESSAGE
            Transport.send(message);

        } catch (Exception ex) {

            Logs.writeLog("(AUTOMATIC FEEDS UPDATE) ERROR SENDING EMAIL MESSAGE UPDATE TO COMMUNITY´S EMAILS : " + ex.getCause(), true);
            return false;
        }

        return true;
    }

    /**
     * CLASS TO HANDLE AUTHETICATION CREDENTIALS
     */
    public class Authentication extends Authenticator {

        private PasswordAuthentication pa;

        /**
         *
         * @param username - Authentication username
         * @param password - Authentication password
         */
        public Authentication(String username, String password) {

            pa = new PasswordAuthentication(username, password);
        }

        /**
         *
         * GETS A 'PasswordAuthentication' OBJECT
         *
         * @return - PasswordAuthentication Object
         */
        @Override
        public PasswordAuthentication getPasswordAuthentication() {
            return pa;
        }
    }

}
