package Dal;

import Utils.Logs;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 *
 * CLASS FOR HANDLING MYSQL DATABASE TRANSACTIONS
 */
public class MYSQL_DB implements DataBaseAccess {

    private Connection connection;
    private Statement smt;
    private ResultSet rs;

    /**
     *
     * OPENS A DATABASE CONNECTION
     *
     * @param db_server - Database server´s address
     * @param db_database - Database´s name
     * @param db_username - Authentication database´s username
     * @param db_password - Authentication database's password
     * @return true if success | false if it fails
     */
    public boolean openConnection(String db_server, String db_database, String db_username, String db_password) {

        try {

            Class.forName("com.mysql.jdbc.Driver");

            this.connection = DriverManager.getConnection("jdbc:mysql://" + db_server + "/" + db_database, db_username, db_password);

        } catch (Exception ex) {

            Logs.writeLog("(AUTOMATIC FEEDS UPDATE) FAILED TO OPEN DATABASE'S CONNECTION : " + ex.getCause(), true);
            return false;
        }

        return true;
    }

    /**
     *
     * CLOSE DATABASE'S CONNECTION
     *
     * @return true if success | false if it fails
     */
    public boolean closeConnection() {

        try {

            this.rs.close();
            this.smt.close();
            this.connection.close();
        } catch (Exception ex) {

            Logs.writeLog("(AUTOMATIC FEEDS UPDATE) FAILED TO CLOSE DATABASE'S CONNECTION : " + ex.getCause(), true);
            return false;
        }

        return true;
    }

    /**
     *
     * PERFORM A SELECT QUERY TO DATABASE
     *
     * @param SQL - Query statement
     * @return - 'null' if it fails | All records of the required information
     */
    public ResultSet select(String SQL) {

        try {

            this.smt = this.connection.createStatement();
            this.rs = smt.executeQuery(SQL);

        } catch (Exception ex) {

            Logs.writeLog("(AUTOMATIC FEEDS UPDATE) FAILED TO PERFORM SELECT QUERY TO DATABASE : " + ex.getCause(), true);
            this.closeConnection();
            return this.rs;
        }

        return this.rs;
    }

    /**
     *
     * PERFORM UPDATE QUERY DATABASE
     *
     * @param SQL - Query statement
     * @return - true if success | false if it fails
     */
    public boolean update(String SQL) {

        try {

            this.smt = this.connection.createStatement();
            this.smt.executeUpdate(SQL);
        } catch (Exception ex) {

            Logs.writeLog("(AUTOMATIC FEEDS UPDATE) FAILED TO PERFORM UPDATE QUERY TO DATABASE : " + ex.getCause(), true);
            this.closeConnection();
            return false;
        }

        return true;
    }
}
