package Dal;

import Utils.Logs;

/**
 *
 * FACTORY CLASS TO CHOOSE A DATABASE OBJECT
 */
public class FactoryDataBaseAccess {

    /**
     *
     * GETS A DATABASE OBJECT
     *
     * @param SGDB - Database management system type
     * @return - Database object
     */
    public static DataBaseAccess getDataBaseAccessObject(String SGDB) {

        DataBaseAccess dbObj = null;

        try {
            dbObj = (DataBaseAccess) Class.forName("Dal." + SGDB + "_DB").newInstance();
        } catch (Exception ex) {

            Logs.writeLog("(AUTOMATIC FEEDS UPDATE) UNABLE TO GET DATABASE CONNECTION'S OBJECT", true);
            return dbObj;
        }

        return dbObj;
    }
}
