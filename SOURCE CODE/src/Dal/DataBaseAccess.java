package Dal;

import java.sql.ResultSet;

/**
 *
 * INTERFACE THAT SETS DATABASE ABSTRACT BEHAVIOURS
 */
public interface DataBaseAccess {

    public boolean openConnection(String db_server, String db_database, String db_username, String db_password);

    public boolean closeConnection();

    public ResultSet select(String SQL);

    public boolean update(String SQL);
}
